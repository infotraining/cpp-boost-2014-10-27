#include <iostream>
#include <vector>
#include <complex>
#include <algorithm>
#include <boost/variant.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_float.hpp>

using namespace std;

struct AbsVisitor : public boost::static_visitor<double>
{
    template <typename T>
    typename boost::disable_if<boost::is_float<T>, double>::type operator()(const T& arg) const
    {
        return abs(arg);
    }

    template <typename T>
    typename boost::enable_if<boost::is_float<T>, double>::type operator()(const T& arg) const
    {
        return fabs(arg);
    }
};

int main()
{
    typedef boost::variant<int, short, double, float, complex<double> > VariantNumber;
    vector<VariantNumber> vars;
    vars.push_back(-1);
    vars.push_back(3.14F);
    vars.push_back(7.22);
    vars.push_back(short{-7});
    vars.push_back(complex<double>(-1, -1));

    // TODO: korzystając z mechanizmu wizytacji wypisać na ekranie moduły liczb
    AbsVisitor abs_visitor;

    transform(vars.begin(), vars.end(),
              ostream_iterator<double>(cout, " "), boost::apply_visitor(abs_visitor));

    cout << endl;
}
