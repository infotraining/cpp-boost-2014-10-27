#include <iostream>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/tuple/tuple_io.hpp>
#include <boost/algorithm/string.hpp>
#include <set>

using namespace std;

template <typename T>
void print_tuple(T& t, string prefix)
{
    cout << prefix << " = " << t << endl;
}

boost::tuple<int, int> divide(int n, int d)
{
    return boost::make_tuple(n/d, n%d);
}

class Person
{
    int id_;
    string name_;
    int age_;

    boost::tuple<const int&, const string&, const int&>  tied() const
    {
        return boost::tie(id_, name_, age_);
    }

public:
    Person(int id, string name, int age) : id_{id}, name_{name}, age_{age}
    {}

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }

    bool operator<(const Person& p) const
    {
        return boost::tie(id_, name_, age_) < boost::tie(p.id_, p.name_, p.age_);
    }
};

int main()
{
	// krotka
	boost::tuple<int, double, string> triple(43, 3.1415, "Krotka...");

	// odwolania do krotki
    cout << boost::tuples::set_open('{') << boost::tuples::set_close('}')
         << "triple = ("
		 << triple.get<0>() << ", "
		 << triple.get<1>() << ", "
		 << boost::tuples::get<2>(triple) << ")" << endl;


    // domyslna inicjalizacja krotki
	boost::tuple<short, bool, string> default_tuple;
	print_tuple(default_tuple, "default_tuple");

	// funkcja pomocnicza - make tuple
	triple = boost::make_tuple(12, 32.222, "Inna krotka...");
	print_tuple(triple, "triple");

	triple.get<2>() = "Inny tekst...";
	print_tuple(triple, "triple");

	// krotki z referencjami
	int x = 10;
	string str = "Tekst...";

	boost::tuple<int&, string&> ref_tpl(x, str);
	ref_tpl.get<0>()++;
	boost::to_upper(ref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

	boost::tuple<const int&, string&> cref_tpl = boost::make_tuple(boost::cref(x), boost::ref(str));

	//cref_tpl.get<0>()++; // Blad! Referencja do stalej!
	boost::to_lower(cref_tpl.get<1>());

	cout << "x = " << x << endl;
	cout << "str = " << str << endl;

    int result, quotient;

    //boost::tuple<int&, int&> temp(result, quotient);
    //temp = divide(5, 2);

    boost::tie(result, quotient) = divide(5, 2);

    cout << "result: " << result << endl;
    cout << "quotient: " << quotient << endl;

    boost::tie(result, boost::tuples::ignore) = divide(7, 3);

    set<int> set1;

    set<int>::iterator where;
    bool was_inserted;

    boost::tie(where, was_inserted) = set1.insert(1);

    if (was_inserted)
        cout << "Wstawiono " << *where << endl;
    else
        cout << *where << " byl juz w kontenerze..." << endl;
}
