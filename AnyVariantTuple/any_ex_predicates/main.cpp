#include <iostream>
#include <vector>
#include <list>
#include <iterator>
#include <functional>
#include <boost/any.hpp>
#include <boost/range.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/range/adaptors.hpp>

using namespace std;

template <typename Type>
struct IsType
{
    typedef bool result_type;
    typedef const boost::any& argument_type;

    bool operator()(const boost::any& a) const
    {
        return a.type() == typeid(Type);
    }
};

class Gadget
{
    int id_;
public:
    Gadget(int id) : id_{id}
    {}

    void info()
    {
        cout << "Gadget(id: " << id_ << ")\n";
    }
};

template <typename InIt, typename Function>
void mfor_each(InIt start, InIt end, Function f)
{
    while (start != end)
        f(*start++);
}

class Formatter
{
public:
    void operator()(const boost::any& a) const
    {
        if (a.type() == typeid(int))
        {
            cout << "<int>" << *boost::unsafe_any_cast<int>(&a) << "</int>" << endl;
        }
        else if (a.type() == typeid(string))
        {
            cout << "<string>" << *boost::unsafe_any_cast<string>(&a) << "</string>" << endl;
        }
    }
};

int main()
{
    vector<Gadget> gadgets = { Gadget{1}, Gadget{6}, Gadget{665} };
    mfor_each(gadgets.begin(), gadgets.end(), mem_fn(&Gadget::info));


	vector<boost::any> store_anything;

	store_anything.push_back(1);
	store_anything.push_back(5);
	store_anything.push_back(string("three"));
	store_anything.push_back(3);
	store_anything.push_back(string("four"));
	store_anything.push_back(string("one"));
	store_anything.push_back(string("eight"));
	store_anything.push_back(5);
	store_anything.push_back(4);
	store_anything.push_back(boost::any());
	store_anything.push_back(string("five"));
	store_anything.push_back(string("six"));
	store_anything.push_back(boost::any());


	/* TO DO :
     * Wykorzystując algorytmy biblioteki standardowej wykonaj nastapujące czynnosci (napisz odpowiednie
	 * do tego celu predykaty lub obiekty funkcyjne):
     * 1 - przefiltruj wartosci niepuste w kolekcji stored_anything
	 * 2 - zlicz ilosc elementow typu int oraz typu string
	 * 3 - wyekstraktuj z kontenera store_anything do innego kontenera wszystkie elementy typu string
	 */

	// 1
	vector<boost::any> non_empty;

    non_empty.push_back(3.14);

//    remove_copy_if(store_anything.begin(), store_anything.end(), back_inserter(non_empty),
//                   mem_fn(&boost::any::empty));
    boost::remove_copy_if(store_anything, back_inserter(non_empty),
                          mem_fn(&boost::any::empty));

//    for(auto it = store_anything.begin(); it != store_anything.end(); ++it)
//        if (!it->empty())
//            non_empty.push_back(*it);

    boost::for_each(non_empty, Formatter());

	cout << "store_anything.size() = " << store_anything.size() << endl;
	cout << "non_empty.size() = " << non_empty.size() << endl;

	// 2
    int count_int = boost::count_if(store_anything, IsType<int>());
	// TODO
    cout << "stored_anything przechowuje " << count_int << " elementow typu int" << endl;

    int count_string = boost::count_if(store_anything, IsType<string>());
	// TODO
    cout << "stored_anything przechowuje " << count_string << " elementow typu string" << endl;

	// 3

    // string any_cast<string>(const boost::any&)
    // string* any_cast<string>(const boost::any*)

    string (*convert)(const boost::any&) = &boost::any_cast;

	list<string> string_items;
    boost::transform(store_anything
                        | boost::adaptors::filtered(IsType<string>())
                        | boost::adaptors::reversed,
                     back_inserter(string_items),
                     convert);

	cout << "string_items: ";
	copy(string_items.begin(), string_items.end(),
			ostream_iterator<string>(cout, " "));
	cout << endl;
}
