#include <stdio.h>
#include <stdexcept>
#include <iostream>
#include <memory>
#include <boost/noncopyable.hpp>

const char* get_line()
{
	static size_t count = 0;

	if (++count == 13)
		throw std::runtime_error("Blad!!!");

	return "Hello RAII\n";
}

class Semaphore
{
public:
    void enter()
    {
        std::cout << "Semaphore::enter()" << std::endl;
    }

    void exit()
    {
        std::cout << "Semaphore::exit()" << std::endl;
    }
};

class CriticalSectionRAII : boost::noncopyable
{
    Semaphore& s_;
public:
    CriticalSectionRAII(Semaphore& s)
    {
        s_.enter();
    }

    ~CriticalSectionRAII()
    {
        s_.exit();
    }
};

void save_to_file(const char* file_name)
{
	FILE* file = fopen(file_name, "w");

	if ( file == 0 )
		throw std::runtime_error("Blad otwarcia pliku!!!");

	for(size_t i = 0; i < 100; ++i)
		fprintf(file, get_line());

	fclose(file);
}

class FileGuard : boost::noncopyable
{
    FILE* file_;

public:
    FileGuard(FILE* file) : file_ {file}
    {
        if (!file_)
            throw std::runtime_error("Blad otwarcia pliku...");
    }

    ~FileGuard()
    {
        fclose(file_);
    }

    FILE* get() const
    {
        return file_;
    }
};

Semaphore global_semaphore;

// TO DO: RAII
void save_to_file_with_raii(const char* file_name)
{
    CriticalSectionRAII cs(global_semaphore);

    FileGuard file(fopen(file_name, "w"));

    //FileGuard copy_of_file = file;

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_shared_ptr(const char* file_name)
{
    std::shared_ptr<FILE> file {fopen(file_name, "w"),
                                [](FILE* f) { if (f) fclose(f); }};

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

void save_to_file_with_unique_ptr(const char* file_name)
{
    auto custom_dealoc = [] (FILE* f) { fclose(f); };

    std::unique_ptr<FILE, decltype(custom_dealoc)> file {fopen(file_name, "w"),
                                                         custom_dealoc};

    if (!file)
        throw std::runtime_error("Blad otwarcia pliku!!!");

    for(size_t i = 0; i < 100; ++i)
        fprintf(file.get(), get_line());
}

int main()
try
{
    //save_to_file("text.txt");
    save_to_file_with_raii("text.txt");
}
catch(const std::exception& e)
{
	std::cout << e.what() << std::endl;

    std::cout << "Press a key..." << std::endl;
    std::string temp;
    std::getline(std::cin, temp);
}
