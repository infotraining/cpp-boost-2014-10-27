#include <iostream>
#include <memory>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <cassert>
#include <vector>
#include <boost/noncopyable.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

using namespace std;

class Gadget : boost::noncopyable
{
public:
    Gadget(int id = 0) : id_ {id}
    {
        cout << "Gadget(id: " << id_ << ")" << endl;
    }

    Gadget(int id, const string& name) : id_ {id}, name_{name}
    {
        cout << "Gadget(id: " << id_
             << ", name: " << name_ << ")" << endl;
    }

    virtual ~Gadget()
    {
        cout << "~Gadget(id: " << id_ << ")" << endl;
    }

    virtual void info() const //const
    {
        cout << "Gadget::info(id=" << id_ << ")" << endl;
    }

protected:
    int id_;
    string name_ = "unknown";
};

class SuperGadget : public Gadget
{
public:
    SuperGadget(int id, const string& name) : Gadget {id, name}
    {
        cout << "SuperGadget(id: " << id_ << ", name: " << name_ << ")" << endl;
    }

    ~SuperGadget()
    {
        cout << "~SuperGadget(id: " << id_ << ")" << endl;
    }

    void info() const override
    {
        cout << "SuperGadget::info(id=" << id_ << ")" << endl;
    }

    void super_only() const
    {
        cout << "SuperGadget::super_only(id=" << id_ << ")" << endl;
    }
};

void use_gadget(Gadget* g)
{
    if (g)
    {
        cout << "Using : ";
        g->info();
    }
}

void use_gadget(Gadget& g)
{
    cout << "Using with auto_ptr: ";
    g.info();
}

void destroy_and_log(Gadget* g)
{
    cout << "Log: Gadget destroyed..." << endl;
    delete g;
}

namespace auto_ptrs
{
    auto_ptr<Gadget> create_gadget(int id)
    {
        return auto_ptr<Gadget>{new Gadget{id}};
    }

    // funkcja typu sink
    void destroy_and_log(auto_ptr<Gadget> g)
    {
        cout << "Log: Gadget destroyed..." << endl;
    }

    void demo()
    {
        auto_ptr<Gadget> g {new Gadget{1}};

        // przeciążone operatory: *, ->
        g->info();
        (*g).info();

        use_gadget(g.get());

        g.reset();
        assert(g.get() == nullptr);

        g.reset(new Gadget{2});
        g->info();

        destroy_and_log(g.release());
        assert(g.get() == nullptr);

        cout << "\n\n";

        g.reset(new Gadget{13});
        g->info();

        auto_ptr<Gadget> copy_of_g = g;

        assert(g.get() == nullptr);
        copy_of_g->info();

        auto_ptr<Gadget> g2 = create_gadget(15);
        g2->info();
        use_gadget(*g2);
        //g2->info(); // undefined behavior

        destroy_and_log(create_gadget(99));

        // auto_ptr<Gadget> tab(new Gadget[10]);  // error - undefined behavior
    }
}

namespace unique_ptrs
{
    unique_ptr<Gadget> create_gadget(int id)
    {
        return unique_ptr<Gadget>{new Gadget{id}};
    }

    // funkcja typu sink
    void destroy_and_log(unique_ptr<Gadget> g)
    {
        cout << "Log: ";
        g->info();
        cout << " destroyed..." << endl;
    }

    void demo()
    {
        unique_ptr<Gadget> g1 {new Gadget{1}};
        g1->info();

        unique_ptr<Gadget> g2 = move(g1);
        g2->info();

        unique_ptr<Gadget> g3 = create_gadget(3);
        g3->info();

        //destroy_and_log(move(g3));
        destroy_and_log(create_gadget(102));

        g3 = create_gadget(103);

        vector<unique_ptr<Gadget>> gadgets;

        gadgets.push_back(move(g3));
        assert(g3.get() == nullptr);

        gadgets.push_back(create_gadget(4));
        gadgets.push_back(unique_ptr<Gadget>{new Gadget{5}});
        gadgets.emplace_back(new Gadget{6});

        cout << "\n\n";
        for(const auto& ptr : gadgets)
            ptr->info();

        unique_ptr<Gadget[]> tab {new Gadget[10]};

        tab[0].info();
    }
}

namespace scoped_ptrs
{
    void demo()
    {
        boost::scoped_ptr<Gadget> ptr { new Gadget{1} };
        ptr->info();

        ptr.reset(new Gadget{2});
        ptr->info();

        boost::scoped_array<Gadget> tab { new Gadget[10] };
        tab[0].info();

        //boost::scoped_ptr<Gadget> copy_ptr = move(ptr);
    }
}

namespace shared_ptrs
{

    namespace impl = boost;

    void demo()
    {
        impl::shared_ptr<Gadget> external = impl::make_shared<Gadget>(1, "ipad");

        {
            auto internal = impl::make_shared<Gadget>(4, "mp3 player");

            vector<impl::shared_ptr<Gadget>> gadgets
                    = { impl::make_shared<Gadget>(2, "glass"), impl::make_shared<Gadget>(3, "mobile") };

            gadgets.push_back(external);
            gadgets.push_back(move(internal));

            for(const auto& g : gadgets)
            {
                g->info();
            }
        }

        cout << "\n\n";

        shared_ptr<Gadget> base_ptr = make_shared<SuperGadget>(99, "laptop");
        base_ptr->info();

        shared_ptr<SuperGadget> derived_ptr = static_pointer_cast<SuperGadget>(base_ptr);
        derived_ptr->super_only();

        Gadget* ptr_base2 = new SuperGadget{88, "notebook"};
        ptr_base2->info();

        SuperGadget* ptr_derived2 = static_cast<SuperGadget*>(ptr_base2);
        ptr_derived2->super_only();

        cout << "Po wyjsciu ze scope" << endl;

        delete ptr_base2;
    }
}

int main()
{   
    //auto_ptrs::demo();

    //unique_ptrs::demo();

    //scoped_ptrs::demo();

    shared_ptrs::demo();
}

