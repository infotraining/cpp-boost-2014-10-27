#include <iostream>
#include <boost/cast.hpp>
#include <boost/lexical_cast.hpp>

using namespace std;

int main()
{
    int x = 1;
    auto sx = boost::numeric_cast<short>(x);
    unsigned int ux = boost::numeric_cast<unsigned int>(x);

    double dx = 0.00000000000000001;
    float fx = boost::numeric_cast<float>(dx);

    cout << "x: " << x << endl;
    cout << "sx: " << sx << endl;
    cout << "ux: " << ux << endl;
    cout << "dx: " << dx << endl;
    cout << "fx: " << fx << endl;


    string str = "Number: " + boost::lexical_cast<string>(x)
                 + "; ux: " + to_string(ux);
    cout << str << endl;

    std::locale::global(std::locale("pl_PL.utf8"));

    str = "78,9";

    cout << "1 + 78 = " << (1 + boost::lexical_cast<double>(str)) << endl;

    string pl_double = boost::lexical_cast<string>(66.6);
    cout << pl_double << endl;

    cout << "value: " << stoi("0xff", 0, 16) << endl;

}

