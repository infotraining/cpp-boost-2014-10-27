#include <iostream>
#include <iterator>
#include <list>
#include <vector>
#include <boost/type_traits/is_pod.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_float.hpp>
#include <cstring>

using namespace std;

// 1 - napisz generyczny algorytm mcopy kopiujący zakres elementow typu T [first, last)
//     do kontenera rozpoczynającego się od dest
template <typename InIt, typename OutIt>
OutIt mcopy(InIt start, InIt end, OutIt out)
{
    cout << "Generic mcopy! " << endl;
    while(start != end)
    {
        *out++ = *start++;
    }

    return out;
}

// 2 - napisz zoptymalizowaną wersję mcopy
//     wykorzystującą memcpy dla tablic T[] gdzie typ T jest typem POD
template <typename T>
typename boost::enable_if<boost::is_pod<T>, T*>::type mcopy(T* start, T* end, T* out)
{
     cout << "Optimized mcopy!" << endl;
     memcpy(out, start, (end-start)*sizeof(T));

     return out + (end-start);
}

template <typename T, typename Enable = void>
class DataProcessor
{
public:
    void process(vector<T>& vec)
    {
        cout << "DataProcessor<T>::process()" << endl;
    }
};

template <typename T>
class DataProcessor<T, typename boost::enable_if<boost::is_float<T> >::type>
{
public:
    void process(vector<T>& vec)
    {
        cout << "DataProcessor<float>::process()" << endl;
    }
};

int main()
{
    string words[] = { "one", "two", "three", "four" };

    list<string> list_of_words(4);

    mcopy(words, words + 4, list_of_words.begin()); // działa wersja generyczna

    mcopy(list_of_words.begin(), list_of_words.end(), ostream_iterator<string>(cout, " "));
    cout << "\n";

    int numbers[] = { 1, 2, 3, 4, 5 };
    int target[5];

    mcopy(numbers, numbers + 5, target); // działa wersja zoptymalizowana

    mcopy(target, target + 5, ostream_iterator<int>(cout, " "));
    cout << "\n";

    vector<int> vec_int;
    vector<double> vec_dbl;

    DataProcessor<int> dp_int;
    dp_int.process(vec_int);

    DataProcessor<double> dp_double;
    dp_double.process(vec_dbl);
}

