#include <iostream>
#include <typeinfo>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_integral.hpp>

using namespace std;

template <typename T>
typename boost::enable_if<boost::is_integral<T>, T>::type foo(const T& arg)
{
    cout << "foo(int: " << arg << ")" << endl;

    return arg;
}

template <typename T>
typename boost::disable_if<boost::is_integral<T> >::type foo(const T& arg)
{
    cout << "foo<T>(" << typeid(T).name() << ": " << arg << ")" << endl;
}

struct Int
{
    int value;

    typedef void nested_type;
};

ostream& operator<<(ostream& out, const Int& i)
{
    out << i.value;

    return out;
}

int main()
{
    foo(7);

    int x = 10;
    cout << foo(x) << endl;

    const short sx = 5;
    cout << foo(sx) << endl;

    double dx = 3.14;
    foo(dx);

    Int i = {44};
    foo(i);
}

