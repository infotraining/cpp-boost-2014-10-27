#ifndef TOBEDELETED_HPP_
#define TOBEDELETED_HPP_

#include <iostream>
#include <vector>

class ToBeDeleted
{
    std::vector<int> vec_;

public:
    ToBeDeleted() : vec_(100, -1)
	{
		std::cout << "Konstruktor ToBeDeleted" << std::endl;
	}

	~ToBeDeleted()
	{
		std::cout << "Destruktor ~ToBeDeleted" << std::endl;
	}
};

#endif /* TOBEDELETED_HPP_ */
