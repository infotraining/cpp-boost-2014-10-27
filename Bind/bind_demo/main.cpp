#include <iostream>
#include <boost/lexical_cast.hpp>
#include <boost/bind.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>
#include <memory>
#include <functional>
#include <boost/range/adaptors.hpp>
#include <boost/range/algorithm.hpp>
#include <boost/function.hpp>

using namespace std;

void foo2(int x, double y)
{
    cout << "foo2(x: " << x << ", y: " << y << ")" << endl;
}

string foo3(int x, int& counter, const string& prefix)
{
    ++counter;
    return prefix + ": " + boost::lexical_cast<string>(x);
}

struct Add
{
    typedef int result_type;

    int operator()(int a, int b) const
    {
        return a + b;
    }
};

class Person
{
    int id_;
    string name_;
    int age_;

    boost::tuple<const int&, const string&, const int&>  tied() const
    {
        return boost::tie(id_, name_, age_);
    }

public:
    Person(int id, string name, int age) : id_{id}, name_{name}, age_{age}
    {}

    ~Person()
    {
        info("~Person");
    }

    bool is_retired(int threshold) const
    {
        return age_ > threshold;
    }

    int age() const
    {
        return age_;
    }

    bool operator==(const Person& p) const
    {
        return tied() == p.tied();
    }

    bool operator<(const Person& p) const
    {
        return boost::tie(id_, name_, age_) < boost::tie(p.id_, p.name_, p.age_);
    }

    void info(const string& prefix) const
    {
        cout << prefix << ": id=" << id_ << "; name=" << name_ << "; age=" << age_ << endl;
    }
};

int main()
{
    boost::function<void (double)> f1 = boost::bind(&foo2, 10, _1);
    f1(3.14);
    f1(5.66);

    auto f2 = boost::bind(&foo2, _1, 3.14);
    f2(66);

    auto f3 = boost::bind(&foo2, _2, _1);
    f3(3.14, 10);

    int x = 10;
    int counter = 0;
    string prefix = "Bind";

    auto f4 = boost::bind(&foo3, x, boost::ref(counter), boost::cref(prefix));
    cout << f4() << " - counter: ";
    cout << counter << endl;

    auto add5 = boost::bind(Add(), _1, 5);

    cout << add5(10) << endl;
    cout << add5(20) << endl;

    // wiazanie z metodami

    Person p{1, "Kowalski", 44};
    boost::shared_ptr<Person> ptr = boost::make_shared<Person>(2, "Nowak", 77);
    unique_ptr<Person> uq_ptr{ new Person{99, "Nijaki", 44}};

    {
        auto local = std::bind(&Person::info, move(uq_ptr), "Osoba");

        local();
    }

    //auto info1 = boost::bind(&Person::info, boost::cref(p), "Osoba");
    //auto info1 = boost::bind(&Person::info, &p, "Osoba");
    auto info1 = boost::bind(&Person::info, ptr, "Osoba");
    info1();

    auto info2 = boost::bind(&Person::info, _1, "Person");

    info2(p);
    info2(&p);
    info2(ptr);

    vector<Person> people = { p, *ptr, Person {6, "Anonim", 88} };

    cout << "\n\n";
    for_each(people.begin(), people.end(), boost::bind(&Person::info, _1, "Osoba"));

    cout << "\n\nRetired:\n";
//    boost::for_each(people
//                    | boost::adaptors::filtered(
//                        boost::bind(&Person::is_retired, _1, 67)),
//                    boost::bind(&Person::info, _1, "Osoba"));

    boost::for_each(people
                    | boost::adaptors::filtered(
                        boost::bind(&Person::age, _1) > 67),
                    boost::bind(&Person::info, _1, "Osoba"));


    cout << "\n\n";
}

